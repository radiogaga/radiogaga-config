# RadioGaGa configs

Ce dépot met à disposition un script permettant de générer les fichiers de configurations pour régler les critères de sélections du récepteur RadioGaGa.  

Dans *filter_coefficients/* se trouvent différentes configurations de filtres. Leur réponse est visible via le script **show_filter.py**  
Dans *configurations_files/* se trouvent différents fichiers d'entrés (.yaml) permettant de régler les différentes choses (filtres, paramètres trigger etc.). Et le script **gen_configuration.py** files permet de sortir les fichiers de conf à copier coller dans la VCR. 

Pour la partie Science, modif trigger et filtres seulement le fichier **recursive_configuration_board_0.conf**  est à copier coller.   
Les deux fichiers d'entrée à modifier sont **configurations_files/input/day.yaml** et **configurations_files/input/night.yaml**. Les autres servent de tests/exemples.

