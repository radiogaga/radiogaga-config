#!/usr/bin/python

import os, re, sys
from numpy import ceil, log2, arange
from shutil import rmtree

import pylab as pl
import numpy as np

out_dir = '../data/config/'
verbose = False


def clog2(x):
    return int(ceil(log2(x)))

def ceil_div(n, d):
    return int(np.ceil(n/d))

def svn_parse_id(id):
    return id.split(' ')

def svn_rev(id):
    return int(svn_parse_id(id)[2])

def svn_date(id):
    return svn_parse_id(id)[3]

def svn_time(id):
    return svn_parse_id(id)[4]

def svn_author(id):
    return svn_parse_id(id)[5]


class VHDL_module:
    def __init__(self,fn_VHDL, dict_in=None, verbose=False):
        if dict_in:
            self.__dict__.update(dict_in.__dict__)
            del self.__dict__['comments']
            self.comments = dict_in.comments
            line_number = max([val[0] for val in self.comments.values()]) + 10

        else:
            self.comments = {}
            self.line_number = 1
        # import constants as class attributes
        constants, comments = self.import_VHDL_constants(fn_VHDL, dict_in=dict_in)
        self.__dict__.update(constants)
        self.comments.update(comments)

        if verbose:
            print("    Found {} items".format(len(constants)))
            for kv in constants.items():
                print("CONSTANT {:<30} := {:<16}".format(*kv))

    def import_VHDL_constants(self, fn_VHDL, dict_in=None):
        """Parse a VHDL file "fn_VHDL" to extract CONSTANTs declarations.
           Returns a dictionnary with constants names/values
           Optionnal dic_in dictionnary contains CONSTANTs from other VHDL packages"""
        if dict_in:  # import CONSTANTS from dic_in into local spacename to evaluate new constants
            for k, v in dict_in.__dict__.items():
                locals()[k] = v

        dic_out = {}
        comments = {}
        with open(fn_VHDL,'r') as f:
            for l in f.readlines():
                l = l.strip()
                if l.lower().find('package')>-1 and l.lower().find('body')>-1:
                    break  # no constants in package bodies
                ret = re.findall(r'^\s*CONSTANT\s+(\S+)\s*:\s*(\S+)\s*:=\s*(.+?)\s*;(?:\s*--\s*(.+)|)\s*$', l)
                self.line_number += 1
                if ret:
                    key, typ, val, cmt = ret[0]
                    comments[key] = (self.line_number, typ, val, cmt)
                    for old, new in (('NOT','not'),('AND','and'),('OR','or'),('TRUE','True'),('FALSE','False')):
                        val = val.replace(old,new)
                    if val.startswith('16#'):
                        val = val[3:-1].replace('_','')  # '_' can be used to separate nibbles
                        val = int(val,16)
                    else:
                        val = eval(val)
                    if typ == 'INTEGER' or typ == 'NATURAL':
                        val = int(val)
                    dic_out[key] = val
                    locals()[key] = val  # used for eval
                    if verbose: print(l, "\n ---> ", key, typ, val, type(dic_out[key]))
                elif verbose: print ('# ', l)
        return dic_out, comments

    def write_C_headers(self, filename=None):
        if filename:
            fid = open(filename, 'w')
        else:
            fid = sys.stdout
        fid.write("""#ifndef CONSTANTDRIVER_H\n#define CONSTANTDRIVER_H\n\n""")
        comments = list(self.comments.items())
        comments.sort(key=lambda x:x[1][0])
        prev_line_number = 1
        for constant, comment in comments:
            val = self.__dict__[constant]
            line_number, typ, exp, cmt = comment
            inc_line_number = line_number - prev_line_number
            prev_line_number = line_number
            if inc_line_number > 1:
                fid.write("\n\n")
            if exp:
                cmt = '  // ' + cmt + ' ####  Computed from: ' + exp
            if typ == 'INTEGER' or typ == 'NATURAL':
                arg = "#define {:35s} ({:10d}){:s}\n".format(constant, val, cmt)
                if constant == "AVM_GDK_PROCESSING_BASE":
                    fid.write('// Already defined in noc.h.  Comment here\n')
                    arg = "// " + arg
                fid.write(arg)
            elif typ == 'STRING':
                fid.write('#define {:35s} "{:10s}"{:s}\n'.format(constant, val, cmt))
        fid.write("""\n#endif /* CONSTANTDRIVER_H */""")

    def write_latex(self, filename=None):
        if filename:
            fid = open(filename, 'w')
        else:
            fid = sys.stdout
        comments = list(self.comments.items())
        comments.sort(key=lambda x:x[1][0])
        corrs = { ('_', ''),
                  ('0', 'Zero'),
                  ('1','One'),
                  ('2','Two'),
                  ('3','Three'),
                  ('4','Four'),
                  ('5','Five'),
                  ('6','Six'),
                  ('7','Seven'),
                  ('8','Eight'),
                  ('9','Nine'),
                 }
        prev_line_number = 1
        for constant, comment in comments:
            val = self.__dict__[constant]
            line_number, typ, exp, cmt = comment
            latex_name = constant.title()
            for corr in corrs:
                latex_name = latex_name.replace(*corr)
            inc_line_number = line_number - prev_line_number
            prev_line_number = line_number
            if inc_line_number > 1:
                fid.write("\n\n")
            if exp:
                cmt = '  %' + cmt + ' ####  Computed from: ' + exp
            if typ == 'INTEGER' or typ == 'NATURAL':
                fid.write("\\newcommand\{:s}{{{:d}}}{:s}\n".format(latex_name, val, cmt))
            elif typ == 'STRING':
                fid.write('\\newcommand\{:s}{{{:}}}{:s}\n'.format(latex_name, val, cmt))






class config:
    """Manage the generation of config files and binary files for the configuration"""
    def __init__(self, outdir='./configs/', N_board = 1, verbose = False):
        self.verbose = verbose
        self.out_dir = outdir
        self.N_board = N_board
        if not os.path.exists(self.out_dir):
            os.mkdir(self.out_dir)
        self.fids = []
        for f in range(self.N_board):
            board_i_dir = os.path.join(self.out_dir,"board%d" % f)
            if not os.path.exists(board_i_dir):
                os.mkdir(board_i_dir)
            fname = os.path.join(board_i_dir, "beh_in.txt")
            fid = open(fname, 'w')
            self.fids.append(fid)
        self.log("Opened %d files in %s" % (self.N_board, self.out_dir))

    def close(self):
        for fid in self.fids:
            fid.close()
        self.log("Close all opened files")

    def cmd(self, msg, boards = None, cmt=None):
        """Write command to boards listed in "boards"
          if boards = None, send command to all boards in system"""
        if boards:
            fids = [self.fids[board] for board in boards]
        else:
            fids = self.fids
            boards = range(len(fids))
        for fid, board in zip(fids,boards):
            if cmt:
                fid.write('# ' + cmt + '\n')
                self.log("Write %s for board %d" % (cmt , board))
            else:
                self.log("Write '%s' for board %d" % (msg , board))
            fid.write(msg + '\n\n')

    def sleep(self, t, boards = None, cmt = None):
        """t is the number of seconds to sleep
           if boards = None, send command to all boards in system"""
        t_ns = int(t*1e9)
        msg = "S %d ns" % (t_ns)
        if cmt: cmt = 'Sleep ' + cmt
        self.cmd(msg, boards, cmt=cmt)

    def action(self, action_id, boards = None, cmt = None):
        """action_id (integer) is the action number set by txt2avm
           See entity txt2avm_lib.txt2avm for more information
           if boards = None, send command to all boards in system"""
        msg = "A %d" % (action_id)
        if cmt: cmt = '"Set action" ' + cmt
        self.cmd(msg, boards, cmt=cmt)

    def wait4trig(self, trig, boards = None, cmt = None):
        """trig (integer) is the trig number to wait for
           See entity txt2avm_lib.txt2avm for more information
           if boards = None, send command to all boards in system"""
        msg = "T %d" % (trig)
        if cmt: cmt = '"wait for trig" ' + cmt
        self.cmd(msg, boards, cmt=cmt)

    def end_simulation(self, boards = None, cmt = None):
        """End simulation"""
        msg = "E"
        if cmt: cmt = 'End of simulation : ' + cmt
        self.cmd(msg, boards, cmt=cmt)


    def write(self, addr, data, boards = None, cmt = None):
        """Writes data @ addr
           if boards = None, send command to all boards in system"""
        msg = "W {0:#010x} {1:#010x}".format(addr, data)
        if cmt: cmt = 'Write ' + cmt
        self.cmd(msg, boards, cmt=cmt)

    def read(self, addr, boards = None, cmt = None):
        """Read data @ addr.  Results is stored in processing_beh_out.txt
           if boards = None, send command to all boards in system"""
        msg = "R {0:#010x}".format(addr)
        if cmt: cmt = 'Read ' + cmt
        self.cmd(msg, boards, cmt=cmt)

    def dump(self, addr, count, stride, data, fn_data, boards = None, cmt = None):
        """Dump datafile @ addr.
           if boards = None, send command to all boards in system"""
        if boards == None:
            boards = range(len(self.fids))
        for board in boards:
            fname = os.path.join(self.out_dir,"board%d" % board, fn_data)
            data.tofile(fname, sep = "")
            msg = "D {0:#010x} {1:d} {2:d} {3:}".format(addr , count , stride , fname)
            if cmt: cmt = 'Dump %s ' % (fn_data) + cmt
            self.cmd(msg, (board,), cmt=cmt)

    def get(self, addr, count, stride, fn_data, boards = None, cmt = None):
        """Dump datafile @ addr.
           if boards = None, send command to all boards in system"""
        if boards == None:
            boards = range(len(self.fids))
        for board in boards:
            fname = os.path.join(self.out_dir,"board%d" % board, fn_data)
            msg = "G {0:#010x} {1:d} {2:d} {3:}".format(addr , count , stride , fname)
            if cmt: cmt = 'Get data into %s ' % (fn_data) + cmt
            self.cmd(msg, boards, cmt=cmt)

    def DIAG_reg0(self, nofsample=0, phase=0, mode=0):
        """Converts parameters to 32-bit config register word"""
        return (nofsample << 16) + (phase << 8) + mode

    def log(self,msg):
        if self.verbose:
            print(msg)

def disp_SS_mem(SS_mem):
    N_lanes, nof_beamlets = SS_mem.shape
    f_ss = pl.figure()
    ax = f_ss.add_subplot(111)
    masked_array = np.ma.array(SS_mem, mask=(SS_mem==32768))
    cmap = pl.cm.jet
    cmap.set_bad('w',1.)
    im = ax.imshow(masked_array,
                   interpolation = 'nearest',
                   extent=(-0.5, nof_beamlets-0.5, -0.5, N_lanes-0.5 ),
                   origin = 'lower',
                   cmap=cmap,
                   vmin=0, vmax=1023)
    pl.colorbar(im)
    pl.axis('tight')
    pl.xlabel('reflet / beamlet')
    ax.set_yticks(np.arange(-0.5, N_lanes-0.5))
    ax.set_yticks(np.arange(0, N_lanes), minor=True)
    ax.set_yticklabels(())
    ax.set_yticklabels(np.arange(0, N_lanes), minor=True)
    ax.grid('on')
    pl.ylabel('Lane / Proc')
    pl.suptitle('SS config memory content')

    # print data values at the bottom of the figure
    def format_coord(col, row):
        col += 0.5
        row += 0.5
        if 0<=col<nof_beamlets and 0<=row<N_lanes:
            P = SS_mem[int(row),int(col)]
            return 'beamlet %d, proc %d, reg=%d'%(col, row, P)
        else:
            return 'no data'

    ax.format_coord = format_coord



def compare_SST(ss_f0, SST, SST_mem):
    """Plot SST computed by model and by simulation
    and compute the abs errors and relative errors"""
    N_MR, _, N_polar = SST_mem.shape
    f4 = pl.figure()
    f4.suptitle('Subband statistics')
    ax1 = f4.add_subplot(1,3,1)
    ax1.set_xlabel('Freq (MHz)')
    ax1.set_ylabel('Power (dB)')
    ax1.set_title('Python model')
    ax2 = f4.add_subplot(2,3,2, sharex=ax1)
    ax2.set_xlabel('Freq (MHz)')
    ax2.set_ylabel('Absolute difference (lin)')
    ax2.set_title('Computation error')
    ax3 = f4.add_subplot(2,3,5, sharex=ax1)
    ax3.set_ylim((0,10))
    ax3.set_xlabel('Freq (MHz)')
    ax3.set_ylabel('Relative difference (%)')
    ax3.set_title('Computation error')
    ax4 = f4.add_subplot(1,3,3, sharex=ax1, sharey=ax1)
    ax4.set_xlabel('Freq (MHz)')
    ax4.set_ylabel('Power (dB)')
    ax4.set_title('QuestaSim')

    pol_names="XY"
    pol_colors="br"
    MR_markers=".o^v"
    marker_style = dict(markersize=10, markeredgewidth=1, fillstyle='none')
    for i in range(N_MR):
        for j in range(N_polar):
            ls = pol_colors[j]+MR_markers[i]+'-'

            questasim = SST_mem[i,:,j]
            model = SST[i,:,j]
            P = 10*np.log10(model)
            P[np.isinf(P)]=-20
            ax1.plot(ss_f0/1e6, P, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

            P = 10*np.log10(questasim)
            P[np.isinf(P)]=-20
            ax4.plot(ss_f0/1e6, P, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

            difference = questasim - model
            ax2.plot(ss_f0/1e6, difference, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

            rel_error = 100 * difference / model
            rel_error[(model == 0) & (questasim == 0)] = 0   # no error
            rel_error[(model == 0) & (questasim != 0)] = 100 # relative error difficult to compute...
            ax3.plot(ss_f0/1e6, rel_error, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

    handles, labels = ax4.get_legend_handles_labels()
    ax4.legend(handles, labels)

    return f4


def compare_phased_ssb(one_bank_of_BF_out, Phased_ss_sim):
    """Plot phased beamlet frames computed by model and by simulation
    and compute the abs errors and relative errors"""
    N_MR, N_beamlets, N_polar = one_bank_of_BF_out.shape
    f = pl.figure()
    f.suptitle('Phased subbands (output of BF modules)')

    ax_Py_re = f.add_subplot(2,3,1)
    ax_Py_re.set_xlabel('beamlet #')
    ax_Py_re.set_ylabel('Real part (lin)')
    ax_Py_re.set_title('Python model')

    ax_Py_im = f.add_subplot(2,3,4, sharex=ax_Py_re)
    ax_Py_im.set_xlabel('beamlet #')
    ax_Py_im.set_ylabel('Imaginary part (lin)')

    ax_Qu_re = f.add_subplot(2,3,3, sharex=ax_Py_re, sharey=ax_Py_re)
    ax_Qu_re.set_xlabel('beamlet #')
    ax_Qu_re.set_ylabel('Real part (lin)')
    ax_Qu_re.set_title('QuestaSim')

    ax_Qu_im = f.add_subplot(2,3,6, sharex=ax_Py_re, sharey=ax_Py_im)
    ax_Qu_im.set_xlabel('beamlet #')
    ax_Qu_im.set_ylabel('Imaginary part (lin)')

    ax_absdiff_re = f.add_subplot(4,3,2, sharex=ax_Py_re)
    ax_absdiff_re.set_xlabel('beamlet #')
    ax_absdiff_re.set_ylabel('Absolute error\non real part (lin)')

    ax_absdiff_im = f.add_subplot(4,3,8, sharex=ax_Py_re)
    ax_absdiff_im.set_xlabel('beamlet #')
    ax_absdiff_im.set_ylabel('Absolute error\non imaginary part (lin)')

    ax_reldiff_re = f.add_subplot(4,3,5, sharex=ax_Py_re)
    ax_reldiff_re.set_xlabel('beamlet #')
    ax_reldiff_re.set_ylabel('Relative error\non real part (%)')
    ax_reldiff_re.set_ylim((-20,20))

    ax_reldiff_im = f.add_subplot(4,3,11, sharex=ax_Py_re)
    ax_reldiff_im.set_xlabel('beamlet #')
    ax_reldiff_im.set_ylabel('Relative error\non imaginary part (%)')
    ax_reldiff_im.set_ylim((-20,20))

    pol_names="XY"
    pol_colors="br"
    MR_markers=".o^v"
    marker_style = dict(markersize=10, markeredgewidth=1, fillstyle='none')
    for i in range(N_MR):
        for j in range(N_polar):
            ls = pol_colors[j]+MR_markers[i]+'-'

            questasim = Phased_ss_sim[i,:,j]
            model = one_bank_of_BF_out[i,:,j]
            ax_Py_re.plot(model.real, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)
            ax_Py_im.plot(model.imag, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)
            ax_Qu_re.plot(questasim.real, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)
            ax_Qu_im.plot(questasim.imag, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

            difference = questasim - model
            ax_absdiff_re.plot(difference.real, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)
            ax_absdiff_im.plot(difference.imag, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

            rel_real_error = 100 * difference.real / model.real
            rel_imag_error = 100 * difference.imag / model.imag
            rel_real_error[(model.real == 0) & (questasim.real == 0)] = 0   # no error
            rel_real_error[(model.real == 0) & (questasim.real != 0)] = 100 # relative error difficult to compute...
            rel_imag_error[(model.imag == 0) & (questasim.imag == 0)] = 0   # no error
            rel_imag_error[(model.imag == 0) & (questasim.imag != 0)] = 100 # relative error difficult to compute...
            ax_reldiff_re.plot(rel_real_error, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)
            ax_reldiff_im.plot(rel_imag_error, ls, label="{}{:d}".format(pol_names[j], i), **marker_style)

    handles, labels = ax_Qu_im.get_legend_handles_labels()
    ax_Qu_im.legend(handles, labels)
    return f

def compare_phased_sum(one_bank_of_BF_out, Phased_ss_sim):
    """Plot beamlet frames computed by model and by simulation
    and compute the abs errors and relative errors"""
    N_beamlets, N_polar = one_bank_of_BF_out.shape
    f = pl.figure()
    f.suptitle('Beamlets (output of processing module (beam_arr))')

    ax_Py_re = f.add_subplot(2,3,1)
    ax_Py_re.set_xlabel('beamlet #')
    ax_Py_re.set_ylabel('Real part (lin)')
    ax_Py_re.set_title('Python model')

    ax_Py_im = f.add_subplot(2,3,4, sharex=ax_Py_re)
    ax_Py_im.set_xlabel('beamlet #')
    ax_Py_im.set_ylabel('Imaginary part (lin)')

    ax_Qu_re = f.add_subplot(2,3,3, sharex=ax_Py_re, sharey=ax_Py_re)
    ax_Qu_re.set_xlabel('beamlet #')
    ax_Qu_re.set_ylabel('Real part (lin)')
    ax_Qu_re.set_title('QuestaSim')

    ax_Qu_im = f.add_subplot(2,3,6, sharex=ax_Py_re, sharey=ax_Py_im)
    ax_Qu_im.set_xlabel('beamlet #')
    ax_Qu_im.set_ylabel('Imaginary part (lin)')

    ax_absdiff_re = f.add_subplot(4,3,2, sharex=ax_Py_re)
    ax_absdiff_re.set_xlabel('beamlet #')
    ax_absdiff_re.set_ylabel('Absolute error\non real part (lin)')

    ax_absdiff_im = f.add_subplot(4,3,8, sharex=ax_Py_re)
    ax_absdiff_im.set_xlabel('beamlet #')
    ax_absdiff_im.set_ylabel('Absolute error\non imaginary part (lin)')

    ax_reldiff_re = f.add_subplot(4,3,5, sharex=ax_Py_re)
    ax_reldiff_re.set_xlabel('beamlet #')
    ax_reldiff_re.set_ylabel('Relative error\non real part (%)')
    ax_reldiff_re.set_ylim((-20,20))

    ax_reldiff_im = f.add_subplot(4,3,11, sharex=ax_Py_re)
    ax_reldiff_im.set_xlabel('beamlet #')
    ax_reldiff_im.set_ylabel('Relative error\non imaginary part (%)')
    ax_reldiff_im.set_ylim((-20,20))

    pol_names="XY"
    for j in range(N_polar):
        questasim = Phased_ss_sim[:,j]
        model = one_bank_of_BF_out[:,j]
        ax_Py_re.plot(model.real,'.-')
        ax_Py_im.plot(model.imag,'.-')
        ax_Qu_re.plot(questasim.real,'.-')
        ax_Qu_im.plot(questasim.imag,'.-')

        difference = questasim - model
        ax_absdiff_re.plot(difference.real,'.-')
        ax_absdiff_im.plot(difference.imag,'.-')

        rel_real_error = 100 * difference.real / model.real
        rel_imag_error = 100 * difference.imag / model.imag
        rel_real_error[(model.real == 0) & (questasim.real == 0)] = 0   # no error
        rel_real_error[(model.real == 0) & (questasim.real != 0)] = 100 # relative error difficult to compute...
        rel_imag_error[(model.imag == 0) & (questasim.imag == 0)] = 0   # no error
        rel_imag_error[(model.imag == 0) & (questasim.imag != 0)] = 100 # relative error difficult to compute...
        ax_reldiff_re.plot(rel_real_error,'.-')
        ax_reldiff_im.plot(rel_imag_error,'.-')

    return f



if __name__ == "__main__":

    print("Parse common_pkg.vhd for CONSTANTs")
    common_pkg_constants = VHDL_module(os.path.expandvars('$HPAPB_ROOT_DIR/src/PROCESSING_MODULES/common/src/vhdl/common_pkg.vhd'), verbose=True)
    print("Parse RSP_constants_pkg.vhd for CONSTANTs")
    RSP_constants = VHDL_module(os.path.expandvars('$HPAPB_ROOT_DIR/src/PROCESSING_MODULES/common/src/vhdl/RSP_constants_pkg.vhd'), dict_in=common_pkg_constants, verbose=True)
    RSP_constants.write_C_headers('RSP_constants.h')
    RSP_constants.write_latex('RSP_constants.tex')

    My_config = config(N_board = 5, verbose = True)
    My_config.sleep(100e-9, cmt = 'mon commentaire')
    My_config.sleep(200e-9, (0,3,2), cmt = 'for boards 0, 3, 2 only')
    My_config.write(0,0xbeef, cmt = '"test write"')
    My_config.write(1,0xbeef)
    My_config.read(0, cmt = '"test read"')
    data = arange(128,dtype='int32')
    My_config.dump(0xbeefbeef, 64, 2, data, 'SS.bin', cmt = '"test dump"')
    My_config.get(0xbeefbeef, 1024, 1, 'SST.bin', cmt = '"test get"')
    My_config.wait4trig(1, cmt = '1')

    My_config.close()
