#! /usr/bin/python3
import yaml,os,argparse
from ressources import RADIOGAGA

def tohex(val, nbits):
  if args.simulation:
    return val
  else:
    return hex((val + (1 << nbits)) % (1 << nbits))[2:]
  #return val

def entete(name,idx=-1):
  chaine = ""
  s_idx = "" if idx==-1 else str(idx)
  chaine+="####################\n"
  chaine+= f"# {name} {s_idx}\n"
  chaine+="####################\n"
  return chaine

def write(address,value):
  if type(value) == str:
    chaine = f"W {tohex(address,32)} {value}\n"
  else:
    chaine = f"W {tohex(address,32)} {tohex(value,16)}\n"
  return chaine



if "HPAPB_ROOT_DIR" in os.environ:
  pass
else:
  print("The env variable $HPAPB_ROOT_DIR doesn't exit. Thanks to add it.")
  print("Output files not generated...")
  quit()

parser = argparse.ArgumentParser()
parser.add_argument("-s","--startup",help="Export as startup configuration file",action="store_true"  )
parser.add_argument("-r","--recursiv",help="Export as recursiv configuration file",action="store_true")
parser.add_argument("-sim","--simulation",help="Export as simulation source file",action="store_true")
parser.add_argument("yaml",help=".yaml file to load paramaters")
#parser.add_argument("outputpath",help="path ot export files")

args = parser.parse_args()

if args.startup and args.recursiv:
  print("You can export only 1 kind of configuration by command")
  print("Output files not generated...")
  quit()
if args.startup:
  print("Do you really want to modify the startup configuration file ? This is irreversible. Y/N ?  ")
  answer = input()
  if answer.upper() == 'N':
    print("Output files not generated...")
    quit()
  



path_rsp  = "common/"
coef_path = "../filter_coefficients"
yaml_file = open(str(args.yaml))
parsed_yaml=yaml.load(yaml_file,Loader=yaml.FullLoader)

path_rsp = os.path.expandvars(path_rsp)
coef_path = os.path.expandvars(coef_path)




common = RADIOGAGA.VHDL_module(os.path.join(path_rsp, 'common_pkg.vhd'))
RSP  = RADIOGAGA.VHDL_module(os.path.join(path_rsp, 'RSP_constants_pkg.vhd'), common)


udp_sender_offset= RSP.c_udp_sender_address_offset


processing_offset=RSP.c_processing_address_offset
radiogaga_offset = processing_offset + RSP.c_radiogaga_address_offset
ipd_offset     = radiogaga_offset + RSP.c_ipd_address_offset
dcb_offset     = radiogaga_offset + RSP.c_dcb_address_offset           
presum_offset  = radiogaga_offset + RSP.c_presum_address_offset
filter_offset  = radiogaga_offset + RSP.c_filter_address_offset
trigger_offset = radiogaga_offset + RSP.c_trigger_address_offset 


#
for board_idx in range(parsed_yaml['nof_boards']):
  output_string =f"#Startup configuration file for board {board_idx}\n\n"

  ##########################
  # For all the boards
  ##########################

  # Filter
  if parsed_yaml.get('filter'):
    for filter_idx,filter in enumerate(parsed_yaml['filter']):
      output_string+=entete("Filter",filter_idx)
      output_string+="# Coef :\n"
      coef = open(os.path.join(coef_path,filter['path']),'r')
      filter_address = filter_offset + filter_idx*0x10000
      # Coef
      for coef_idx, line in enumerate(coef):
        output_string += write(int(filter_address) + int(coef_idx),int(line))
      # Shift
      output_string += f"# Shift :\n"
      output_string += write(int(filter_address)+RSP.c_filter_shift_offset,filter['shift'])
      # Name of the filter in trigger register for frame
      # Encode name of the filter 
      # Rej MSB(bit n°15) = 0, 14-0 value of freq
      # Band pass MSB(bit n°15) = 1 14-8 low freq, 6-0 high freq  
      filter_name = filter['path'].split("/")[-1]

      if(filter_name.find('band') != -1):
        lowfreq = int(filter_name.split('_')[0])
        highfreq = int(filter_name.split('_')[1])
        print(lowfreq,highfreq)
        filter_name_coded = (1 << 15) | (lowfreq <<8) | (highfreq) << 0
        print("Passband", filter_name_coded )

      elif (filter_name.find('rej') != -1):
        freq = int(filter_name.split('_')[0])
        filter_name_coded = freq
        print("Rejecteur", filter_name_coded )
      output_string += write(int(trigger_offset)+RSP.c_trigger_filters_offset+filter_idx,filter_name_coded)

  # Presum
  if parsed_yaml.get('presum'):
    output_string+=entete('Presum')
    for chan in range(4):
      output_string+=write(presum_offset+RSP.c_presum_ant_sel_x_offset+chan,parsed_yaml['presum'][f'Board_{board_idx}'][f'Channel_{chan}'])
      output_string+=write(presum_offset+RSP.c_presum_ant_sel_y_offset+chan,parsed_yaml['presum'][f'Board_{board_idx}'][f'Channel_{chan}'])



  ##########################
  # only on the master board
  ##########################
  if (board_idx==parsed_yaml['master_board']):
    
    # Trigger X
    if parsed_yaml.get('trigger_x'):
      output_string+=entete("Trigger")
      for key,value in parsed_yaml['trigger_x'].items():
        output_string+= f"# {key}\n"
        address=eval(f"RSP.c_trigger_{key}_offset")
        output_string+= write(trigger_offset+address,value)

     # Trigger Y
    if parsed_yaml.get('trigger_y'):
      output_string+=entete("Trigger")
      for key,value in parsed_yaml['trigger_y'].items():
        output_string+= f"# {key}\n"
        address=eval(f"RSP.c_trigger_{key}_offset")
        output_string+= write(trigger_offset+address+1,value)

    # UPD_sender
    if parsed_yaml.get('udp_sender'):
      output_string+=entete("UDP_Sender")
      udp_sender = parsed_yaml['udp_sender']
      # MAC
      output_string+="# Mac\n"
      mac = udp_sender['mac']
      mac=mac.replace(':','')
      mac_low =  mac[-8:]
      mac_high = mac[:4]
      output_string+=write( udp_sender_offset+ RSP.c_UDP_sender_lo_32_mac_offset ,mac_low)
      output_string+=write( udp_sender_offset+ RSP.c_UDP_sender_hi_16_mac_offset ,mac_high)
      # IP
      output_string+="# IP\n"
      ip = udp_sender['ip'].split('.')
      ip_hex ='{:02X}{:02X}{:02X}{:02X}'.format(*map(int, ip))
      output_string+=write(udp_sender_offset+ RSP.c_UDP_sender_ip_offset,ip_hex)
      # Port
      output_string+="# Port\n"
      output_string+=write(udp_sender_offset+ RSP.c_UDP_sender_port_offset,udp_sender['port'])
      # Delay
      output_string+="# Delay\n"
      output_string+=write(udp_sender_offset+ RSP.c_UDP_sender_delay_ns_offset,udp_sender['delay'])
      # enable
      output_string+="# enable\n"
      output_string+=write(udp_sender_offset+ RSP.c_UDP_sender_enable_offset,udp_sender['enable'])

  ##########################
  # only on the last board
  ##########################
  if (board_idx==parsed_yaml['nof_boards']-1):
    # SUMX
    output_string+=entete("Sumx")
    output_string+=write(RSP.c_sumX_sum_en_offset,0)


  output_path = "mnt/"
  if not args.simulation:
    output_file = "output/startup_configuration_board" if args.startup else "output/recursive_configuration_board" 
    with open(f"{output_file}_{board_idx}.conf",'w') as f:
      f.write(output_string) 
    print(f"Files {output_file}_{board_idx}.conf generated")
  else : 
    output_file = "output/sim/startup_configuration_board" 
    with open(f"{output_file}_{board_idx}.conf",'w') as f:
      f.write(output_string) 
    print(f"Files {output_file}_{board_idx}.conf generated")



