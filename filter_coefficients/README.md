# Filter coefficients

To generate coefficients we use pyFDA, it's a open source python program. To dowload it : 
``` console
pip install pyfda
```
And to launch it : 
``` console
pyfdax
```
There is more information in RadioGaGa documentation.