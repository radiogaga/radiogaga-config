import sys
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("coef_file", help="path to b coefficient file")
parser.add_argument("shift", help="shift to apply after filtering", type=int)
args = parser.parse_args()

coef_file = args.coef_file
shift = args.shift

b = np.fromfile(coef_file, dtype=int, sep='\n')
Fs = 200e6  # 200 MHz

f, ax = plt.subplots()
ax.plot(b, '.')
ax.set_xlabel('Coef #')
ax.set_ylabel('Coef Amplitude')

# calcule la réponse fréquentielle du filtre
w, h = signal.freqz(b, fs=Fs)

h = h / 2**shift

fig, ax1 = plt.subplots()
ax1.set_title('Digital filter frequency response')
ax1.plot(w/1e6, 20 * np.log10(abs(h)), 'b')
ax1.set_ylabel('Amplitude [dB]', color='b')
ax1.set_xlabel('Frequency [MHz]')

ax2 = ax1.twinx()
angles = np.unwrap(np.angle(h))
ax2.plot(w/1e6, angles, 'g')
ax2.set_ylabel('Angle (radians)', color='g')
ax2.grid()


fig, ax1 = plt.subplots()
ax1.set_title('Digital filter frequency response')
ax1.plot(w/1e6, abs(h), 'b')
ax1.set_ylabel('Amplitude [Lin]', color='b')
ax1.set_xlabel('Frequency [MHz]')

ax2 = ax1.twinx()
angles = np.unwrap(np.angle(h))
ax2.plot(w/1e6, angles, 'g')
ax2.set_ylabel('Angle (radians)', color='g')
ax2.grid()




plt.show()